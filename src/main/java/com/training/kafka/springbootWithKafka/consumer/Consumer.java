package com.training.kafka.springbootWithKafka.consumer;

import java.io.IOException;

public interface Consumer {

    void consume(String c);
}
